"use strict";

window.addEventListener('DOMContentLoaded', function () {
  // START OF: is mobile =====
  function isMobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }
  // ===== END OF: is mobile

  const BODY = $('body');

  // START OF: mark is mobile =====
  (function() {
    BODY.addClass('loaded');

    if(isMobile()){
      BODY.addClass('body--mobile');
    }else{
      BODY.addClass('body--desktop');
    }
  })();
  // ===== END OF: mark is mobile

  let rangeSlider = function(){
    let slider = $('.range'),
      range = $('.range__input'),
      value = $('.range__value');

    slider.each(function(){

      value.each(function(){
        var value = $(this).prev().attr('value');
        $(this).html(value);
      });

      range.on('input', function(){
        $(this).next(value).html(this.value);
      });
    });
  };

  rangeSlider();
});

$(document).ready(function () {
  let parallax = function () {
    let scene = $('.scene').get(0);
    let parallaxInstance = new Parallax(scene);
  }

  parallax();

  setTimeout(function(){
    $('.animation').one('inview', function(event, isInView, visiblePartX, visiblePartY){
      if (isInView === true) {
        $(this).addClass('inview');
      }
    });
  }, 200)
})